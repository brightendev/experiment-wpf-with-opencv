#include "human_detection.h"
#include <fstream>
//#include <iostream>

using namespace std;
using namespace cv;
using namespace dnn;

// Initialize the parameters
float confThreshold = 0.5; // Confidence threshold
float nmsThreshold = 0.4;  // Non-maximum suppression threshold
int inpWidth = 416;  // Width of network's input image
int inpHeight = 416; // Height of network's input image
vector<string> classes;

HumanDetection::HumanDetection(Mode mode)
{
	filePath fp = {};
	initFilePathSelector(mode, fp);
	
	ifstream ifs(fp.classes.c_str());
	string line;
	while (getline(ifs, line)) classes.push_back(line);

	net = readNetFromDarknet(fp.config, fp.weights);
	net.setPreferableBackend(DNN_BACKEND_OPENCV);
	net.setPreferableTarget(DNN_TARGET_OPENCL);

}

HumanDetection::HumanDetection(std::string classesFilePath, std::string configFilePath, std::string weightsFilePath)
{

	ifstream ifs(classesFilePath.c_str());
	string line;
	while (getline(ifs, line)) classes.push_back(line);

	net = readNetFromDarknet(configFilePath, weightsFilePath);
	net.setPreferableBackend(DNN_BACKEND_OPENCV);
	net.setPreferableTarget(DNN_TARGET_CPU);

}

void HumanDetection::test()
{
	unsigned char data[] = { 0, 0, 255, 0, 0, 255, 255, 255, 255, 255, 255, 255 };
	Mat mat(Size(2, 2), CV_8UC3, data);
	std::cout << mat << std::endl;
}

void HumanDetection::detect(Mat& frame)
{
	Mat blob;

	blobFromImage(frame, blob, 1 / 255.0, Size(inpWidth, inpHeight), Scalar(0, 0, 0), true, false);
	net.setInput(blob);

	// Runs the forward pass to get output of the output layers
	vector<Mat> outs;
	net.forward(outs, getOutputsNames(net));

	// Remove the bounding boxes with low confidence
	postProcess(frame, outs);

	// Put efficiency information. The function getPerfProfile returns the overall time for inference(t) and the timings for each of the layers(in layersTimes)
	vector<double> layersTimes;
	double freq = getTickFrequency() / 1000;
	double t = net.getPerfProfile(layersTimes) / freq;
	string label = format("Inference time for a frame : %.2f ms", t);
	putText(frame, label, Point(0, 15), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0, 0, 255));

	// Write the frame with the detection boxes
	// Mat detectedFrame;
	// frame.convertTo(detectedFrame, CV_8U);
	// imwrite("D:/Development/workspace/test/WpfWithOpenCv/output.jpg", frame);

	
}



void HumanDetection::setParkingSlotsPosition(std::vector<ParkingSlot>* slots)
{
	parkingSlots = *slots;
}

void HumanDetection::checkParkingSlot()
{
	for(int i = 0; i < parkingSlots.size(); i++)
	{
		int xTopLeft = parkingSlots[i][0].x;
		int yTopLeft = parkingSlots[i][0].y;
	
		std::cout << "top left x=" + std::to_string(xTopLeft) << std::endl;
		std::cout << "top left y=" + std::to_string(yTopLeft) << std::endl;
	}
}

vector<string> HumanDetection::getOutputsNames(const Net& net)
{
	static vector<string> names;

	// if (names.empty())
	// {
		//Get the indices of the output layers, i.e. the layers with unconnected outputs
		vector<int> outLayers = net.getUnconnectedOutLayers();

		//get the names of all the layers in the network
		vector<cv::String> layersNames = net.getLayerNames();

		// Get the names of the output layers in names
		names.resize(outLayers.size());
		for (size_t i = 0; i < outLayers.size(); ++i) {
			names[i] = layersNames[outLayers[i] - 1];
		}
	// }
	return names;
}

void HumanDetection::postProcess(Mat& frame, const std::vector<Mat>& outs)
{
	vector<int> classIds;
	vector<float> confidences;
	vector<Rect> boxes;

	for (size_t i = 0; i < outs.size(); ++i)
	{
		// Scan through all the bounding boxes output from the network and keep only the
		// ones with high confidence scores. Assign the box's class label as the class
		// with the highest score for the box.
		float* data = (float*)outs[i].data;
		for (int j = 0; j < outs[i].rows; ++j, data += outs[i].cols)
		{
			Mat scores = outs[i].row(j).colRange(5, outs[i].cols);
			Point classIdPoint;
			double confidence;
			// Get the value and location of the maximum score
			minMaxLoc(scores, 0, &confidence, 0, &classIdPoint);
			if (confidence > confThreshold)
			{
				int centerX = (int)(data[0] * frame.cols);
				int centerY = (int)(data[1] * frame.rows);
				int width = (int)(data[2] * frame.cols);
				int height = (int)(data[3] * frame.rows);
				int left = centerX - width / 2;
				int top = centerY - height / 2;

				classIds.push_back(classIdPoint.x);
				confidences.push_back((float)confidence);
				boxes.push_back(Rect(left, top, width, height));

				// draw parking slot
				for (int s = 0; s < parkingSlots.size(); s++)
				{
					double test = pointPolygonTest(parkingSlots[s], Point2f((float)centerX, (float)centerY), true);
					if (test > 0)
					{
						polylines(frame, parkingSlots[s], true, Scalar(0, 0, 255), 2);
					}
					else polylines(frame, parkingSlots[s], true, Scalar(144, 238, 144), 2);
				}
				
			}
		}
	}

	// Perform non maximum suppression to eliminate redundant overlapping boxes with
	// lower confidences
	vector<int> indices;
	NMSBoxes(boxes, confidences, confThreshold, nmsThreshold, indices);
	for (size_t i = 0; i < indices.size(); ++i)
	{
		int idx = indices[i];
		Rect box = boxes[idx];

		// if (classes[classIds[idx]] != "car") continue;
		
		drawPred(classIds[idx], confidences[idx], box.x, box.y,
			box.x + box.width, box.y + box.height, frame);
	}
}

void HumanDetection::drawPred(int classId, float conf, int left, int top, int right, int bottom, Mat& frame)
{
	//Draw a rectangle displaying the bounding box
	rectangle(frame, Point(left, top), Point(right, bottom), Scalar(255, 178, 50), 3);

	// //Get the label for the class name and its confidence
	// string label = format("%.2f", conf);
	// if (!classes.empty())
	// {
	// 	CV_Assert(classId < (int)classes.size());
	// 	label = classes[classId] + ":" + label;
	// }
	//
	// //Display the label at the top of the bounding box
	// int baseLine;
	// Size labelSize = getTextSize(label, FONT_HERSHEY_SIMPLEX, 0.5, 1, &baseLine);
	// top = max(top, labelSize.height);
	// rectangle(frame, Point(left, top - round(1.5 * labelSize.height)), Point(left + round(1.5 * labelSize.width), top + baseLine), Scalar(255, 255, 255), FILLED);
	// putText(frame, label, Point(left, top), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(0, 0, 0), 1);
}

void HumanDetection::initFilePathSelector(Mode mode, filePath& fp)
{
	switch (mode) {
		case DETECT_LQ:
			fp.classes = "./resources/object_detection/coco.names";
			fp.config = "./resources/object_detection/yolov3-tiny.cfg";
			fp.weights = "./resources/object_detection/yolov3-tiny.weights";
			return;
		case DETECT_HQ:
			fp.classes = "";
			fp.config = "";
			fp.weights = "";
	}
}
