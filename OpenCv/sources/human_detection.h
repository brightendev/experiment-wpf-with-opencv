#pragma once
#include <opencv2/opencv.hpp>

// using namespace cv;
// using namespace dnn;

enum Mode
{
	DETECT_LQ = 1,
	DETECT_HQ = 2
};

struct filePath
{
	std::string classes;
	std::string config;
	std::string weights;
};



typedef std::array<cv::Point, 4> ParkingSlot;

class HumanDetection
{
public:
	HumanDetection(Mode mode = DETECT_LQ);
	HumanDetection(std::string classesFilePath, std:: string configFilePath, std::string weightsFilePath);
	static void test();
	void detect(cv::Mat& frame);
	void setParkingSlotsPosition(std::vector<ParkingSlot>* slots);
	void checkParkingSlot();

private:
	cv::dnn::Net net;
	std::vector<ParkingSlot> parkingSlots;
	void initFilePathSelector(Mode mode, filePath& fp);
	std::vector<std::string> getOutputsNames(const cv::dnn::Net& net);
	void postProcess(cv::Mat& frame, const std::vector<cv::Mat>& outs);
	void drawPred(int classId, float conf, int left, int top, int right, int bottom, cv::Mat& frame);
};

