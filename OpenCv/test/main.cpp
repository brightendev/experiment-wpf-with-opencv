#include "../sources/human_detection.h"

int main()
{

	HumanDetection obj("./resources/coco.names", "./resources/yolov3.cfg", "./resources/yolov3.weights");
	cv::Mat img = cv::imread("./resources/test/parking01.jpg");
	obj.detect(img);

	return 0;
}