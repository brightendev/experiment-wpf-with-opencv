﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfWithOpenCv
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
    struct Vertex
    {
        public int x; public int y;

        public Vertex(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
    struct BoundingBox
    {
        public Vertex TopLeft;
        public Vertex TopRight;
        public Vertex BottomLeft;
        public Vertex BottomRight;

        public BoundingBox(Vertex topLeft, Vertex topRight, Vertex bottomLeft, Vertex bottomRight)
        {
            TopLeft = topLeft;
            TopRight = topRight;
            BottomLeft = bottomLeft;
            BottomRight = bottomRight;
        }
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            LoadNet();

            BoundingBox[] bboxes = {
                new BoundingBox(new Vertex(10, 10), new Vertex(30, 10), new Vertex(10, 80), new Vertex(30, 80)),
                new BoundingBox(new Vertex(100, 10), new Vertex(200, 12), new Vertex(100, 200), new Vertex(215, 180)),
//                new BoundingBox(new Vertex(17, 18), new Vertex(19, 20), new Vertex(21, 22), new Vertex(23, 24)),
//                new BoundingBox(new Vertex(25, 26), new Vertex(27, 28), new Vertex(29, 30), new Vertex(31, 32))
            };

            SetParkingSlotsPosition(2, bboxes);

//            CheckParking();
        }


        [DllImport(@"D:\Development\workspace\test\WpfWithOpenCv\Wpf\Dll\OpenCv.dll")]
        private static extern int detect(byte[] mat, uint width, uint height, [MarshalAs(UnmanagedType.FunctionPtr)] DetectCallback callbackPointer);
        [DllImport(@"D:\Development\workspace\test\WpfWithOpenCv\Wpf\Dll\OpenCv.dll", EntryPoint = "loadNet")]
        private static extern int LoadNet();
        [DllImport(@"D:\Development\workspace\test\WpfWithOpenCv\Wpf\Dll\OpenCv.dll", EntryPoint = "setParkingSlotsPosition")]
        private static extern void SetParkingSlotsPosition(uint count, BoundingBox[] bboxes);
        [DllImport(@"D:\Development\workspace\test\WpfWithOpenCv\Wpf\Dll\OpenCv.dll", EntryPoint = "checkParking")]
        private static extern void CheckParking();

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        private delegate void DetectCallback(IntPtr matPtr);

        private void btnPlay_Click(object sender, RoutedEventArgs e)
        {
            video.LoadedBehavior = MediaState.Manual;
            video.Play();
        }

        private void btnPause_Click(object sender, RoutedEventArgs e)
        {
            video.Pause();
        }

        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            video.Stop();
        }

        private void btnDetect_Click(object sender, RoutedEventArgs e)
        {
            video.LoadedBehavior = MediaState.Manual;
            video.Play();

            Thread test = new Thread(new ThreadStart(Detect));
            test.Start();

        }


        private void Detect()
        {
            while(true) {
                ImgData data = Dispatcher.Invoke(() => {
                    RenderTargetBitmap bmp = generateBitmap();

                    int stride = (bmp.PixelWidth * bmp.Format.BitsPerPixel + 7) / 8;
                    byte[] pixelData = new byte[bmp.PixelWidth * bmp.PixelHeight * 4];

                    bmp.CopyPixels(pixelData, stride, 0);

                    return new ImgData() {
                        Width = (uint) bmp.Width,
                        Height = (uint) bmp.Height,
                        PixelData = pixelData,
                        Stride = stride
                    };
                });

                detect(data.PixelData, data.Width, data.Height, (matPtr) => {

                    byte[] mat = new byte[data.Width * data.Height * 4];
                    Marshal.Copy(matPtr, mat, 0, mat.Length - 1);

                    DrawOutput((int) data.Width, (int) data.Height, mat, data.Stride);
                });
            }
        }


        private RenderTargetBitmap generateBitmap()
        {
            Size dpi = new Size(96, 96);
            RenderTargetBitmap bmp = new RenderTargetBitmap((int) video.Width, (int) video.Height, dpi.Width, dpi.Height, PixelFormats.Pbgra32);

            DrawingVisual dv = new DrawingVisual();
            using (DrawingContext ctx = dv.RenderOpen())
            {
                VisualBrush vb = new VisualBrush(video);
                ctx.DrawRectangle(vb, null, new Rect(new Point(0, 0), new Point(video.Width, video.Height)));
            }
            bmp.Render(dv);

            return bmp;
        }

        private void saveBmpToFile(RenderTargetBitmap bmp)
        {
            FileStream stream = new FileStream(@"D:\Development\workspace\test\WpfWithOpenCv\Wpf\test.jpg", FileMode.Create);
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bmp));
            encoder.Save(stream);

            stream.Close();
        }

        private void DrawOutput(int width, int height, byte[] pixelData, int stride)
        {
            Output.Dispatcher.Invoke(() => {
                
                BitmapSource bmpSource = BitmapSource.Create(width, height, 96, 96, PixelFormats.Pbgra32, null, pixelData, stride);
                Output.Width = width;
                Output.Height = height;
                Output.Source = bmpSource;
            });
        }

        private void Video_OnEnded(object sender, RoutedEventArgs e)
        {
            video.Position = new TimeSpan(0, 0, 1);
//            video.Play();
        }

        private void Video_OnMediaOpened(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Video is load");
            
            video.ScrubbingEnabled = true;
            video.Position += new TimeSpan(0, 0, 1);
        }

        private int clickCount = 0;
        private List<Point> slotVertex;
        private List<List<Point>> polygons = new List<List<Point>>();
        private void Video_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            Point clickPoint = e.GetPosition(video);
            Console.WriteLine("position = "+clickPoint);

            if(clickCount == 0) {
                slotVertex = new List<Point>(4);
                slotVertex.Add(clickPoint);
            }
            else if(clickCount > 0 && clickCount < 4) {
                slotVertex.Add(clickPoint);

                if (clickCount == 3) {
                    polygons.Add(slotVertex);
                    btnAddSlot.IsEnabled = true;

                    clickCount = -1;
                }
            }
            
            clickCount++;
        }

        private void BtnAddSlot_OnClick(object sender, RoutedEventArgs e)
        {
            clickCount = 0;

            btnAddSlot.IsEnabled = false;

            DrawSlots();

            BoundingBox[] bboxes = new BoundingBox[polygons.Count];
            for(int i = 0; i < bboxes.Length; i++) {
                bboxes[i] = new BoundingBox(
                    new Vertex((int) polygons[i][0].X, (int) polygons[i][0].Y),
                    new Vertex((int) polygons[i][1].X, (int) polygons[i][1].Y),
                    new Vertex((int) polygons[i][3].X, (int) polygons[i][3].Y),
                    new Vertex((int) polygons[i][2].X, (int) polygons[i][2].Y)
                );
            }
            SetParkingSlotsPosition((uint) bboxes.Length, bboxes);
        }

        private void DrawSlots()
        {
            
            Size dpi = new Size(96, 96);
            RenderTargetBitmap bmp = new RenderTargetBitmap((int) video.Width, (int)video.Height, dpi.Width, dpi.Height, PixelFormats.Pbgra32);
            DrawingVisual dv = new DrawingVisual();
            using (DrawingContext ctx = dv.RenderOpen())
            {
                VisualBrush vb = new VisualBrush(video);
                ctx.DrawRectangle(vb, null, new Rect(new Point(0, 0), new Point(video.Width, video.Height)));
                
                // draw polygons
                foreach(var poly in polygons) {
                    StreamGeometry streamGeometry = new StreamGeometry();
                    using (StreamGeometryContext geometryContext = streamGeometry.Open())
                    {
                        geometryContext.BeginFigure(poly[0], false, true);
                        PointCollection points = new PointCollection {
                            poly[1], poly[2], poly[3],
                        };
                        geometryContext.PolyLineTo(points, true, true);
                    }

                    ctx.DrawGeometry(Brushes.Black, new Pen(Brushes.LightGreen, 2), streamGeometry);

                }
            }
            bmp.Render(dv);
            Output.Width = bmp.Width;
            Output.Height = bmp.Height;
            Output.Source = bmp;

        }
    }

    class ImgData
    {
        public uint Width { get; set; }
        public uint Height { get; set; }
        public byte[] PixelData { get; set; }
        public int Stride { get; set; }
    }
}

