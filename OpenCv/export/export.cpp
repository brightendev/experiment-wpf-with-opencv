

#include <opencv2/imgcodecs.hpp>
#include "../sources/human_detection.h"

using namespace  cv;

int x = 0;
static HumanDetection* detector;

typedef void(__stdcall* DetectCallback)(unsigned char* mat);

extern "C"
{
	
	__declspec(dllexport) void  testDetect() {
		std::cout << "fiew" << std::endl;
		HumanDetection obj("D:/Development/workspace/test/WpfWithOpenCv/OpenCv/resources/coco.names", 
			"D:/Development/workspace/test/WpfWithOpenCv/OpenCv/resources/yolov3.cfg", 
			"D:/Development/workspace/test/WpfWithOpenCv/OpenCv/resources/yolov3.weights");
		cv::Mat img = cv::imread("D:/Development/workspace/test/WpfWithOpenCv/OpenCv/resources/test/parking01.jpg");
		obj.detect(img);
	}

	__declspec(dllexport) void increaseX()
	{
		x++;

		std::cout << x << std::endl;
	}

	__declspec(dllexport) void loadNet()
	{
		detector = new HumanDetection("D:/Development/workspace/test/WpfWithOpenCv/OpenCv/resources/coco.names",
			"D:/Development/workspace/test/WpfWithOpenCv/OpenCv/resources/yolov3.cfg",
			"D:/Development/workspace/test/WpfWithOpenCv/OpenCv/resources/yolov3.weights");
	}

	
	struct Vertex  // use for export
	{
		int x;
		int y;
	};
	struct BoundingBox  // use for export
	{
		Vertex topLeft;
		Vertex topRight;
		Vertex bottomLeft;
		Vertex bottomRight;
	};
	__declspec(dllexport) void setParkingSlotsPosition(const unsigned int count, BoundingBox bboxes[])
	{

		std::vector<ParkingSlot>* slots = new std::vector<ParkingSlot>;
		
		for(int i = 0; i < count; i++) {
			slots->push_back(ParkingSlot{
				Point(bboxes[i].topLeft.x, bboxes[i].topLeft.y),
				Point(bboxes[i].topRight.x, bboxes[i].topRight.y),
				Point(bboxes[i].bottomRight.x, bboxes[i].bottomRight.y),
				Point(bboxes[i].bottomLeft.x, bboxes[i].bottomLeft.y)
				
			});

			int xTopLeft = bboxes[i].topLeft.x;
			int yTopLeft = bboxes[i].topLeft.y;

			std::cout << "top left x=" + std::to_string(xTopLeft) << std::endl;
			std::cout << "top left y=" + std::to_string(yTopLeft) << std::endl;
		}

		detector->setParkingSlotsPosition(slots);
		//
		// delete slots;
	}

	__declspec(dllexport) void checkParking()
	{
		detector->checkParkingSlot();
	}

	__declspec(dllexport) void detect(unsigned char* matrix, unsigned int width, unsigned int height, DetectCallback progressCallback)
	{
		
		Mat* mat = new Mat(Size(width, height), CV_8UC4, matrix);
		cvtColor(*mat, *mat, COLOR_BGRA2BGR);

		detector->detect(*mat);

		cvtColor(*mat, *mat, COLOR_BGR2BGRA);
		
		progressCallback(mat->data);
	}
}
